// Request will be proxied via /api/demo/[...apiGateway].ts
const baseUrl = '/api/demo';

export const BackendApiUrl = {
    test: baseUrl + '/api/test'
}
